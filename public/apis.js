
exports.path = {
    "/createClaim": require('./models/createClaim')["createClaim"],
    "/claimType": require('./models/claimType')["claimType"],
    "/claimStatus": require('./models/claimStatus')["claimStatus"],
    "/sourceType": require('./models/sourceType')["sourceType"],
    "/documentType": require('./models/documentType')["documentType"],
    "/maritalStatus": require('./models/maritalStatus')["maritalStatus"],
    "/occupations": require('./models/occupations')["occupations"],
    "/wagesFrequency": require('./models/wagesFrequency')["wagesFrequency"],
    "/injuryCodes": require('./models/injuryCodes')["injuryCodes"],
    "/bodyCodes": require('./models/bodyCodes')["bodyCodes"],
    "/naicCodes": require('./models/naicCodes')["naicCodes"],
    "/ncciCodes": require('./models/ncciCodes')["ncciCodes"],
    "/injuryCauses": require('./models/injuryCauses')["injuryCauses"],
    "/productType": require('./models/productType')["productType"],
    "/premisesType": require('./models/premisesType')["premisesType"],
    "/countries": require('./models/countries')["countries"],
    "/states": require('./models/states')["states"]
}