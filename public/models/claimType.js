exports.claimType = {
    "get": {
        "tags": [
          "lookups"
        ],
        "summary": "Fetch Claim Types",
        "description": "Get the list of claim type",
        "operationId": "claimType",
        "responses": {
          "200": {
            "description": "List of claim type",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/claimType"
                  }
                }
              }
            }
          },
          "400": {
            "description": "bad input parameter"
          }
        }
    }
}