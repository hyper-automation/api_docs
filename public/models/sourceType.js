exports.sourceType = {
    "get": {
        "tags": [
          "lookups"
        ],
        "summary": "Fetch Source Type",
        "description": "Get the list of Source Type",
        "operationId": "sourceType",
        "responses": {
          "200": {
            "description": "List of Source Type",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/sourceType"
                  }
                }
              }
            }
          },
          "400": {
            "description": "bad input parameter"
          }
        }
    }
}