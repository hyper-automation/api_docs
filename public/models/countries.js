exports.countries = {
    "get": {
        "tags": [
          "lookups"
        ],
        "summary": "Fetch Countries",
        "description": "Get the list of countries",
        "operationId": "countries",
        "responses": {
          "200": {
            "description": "List of countries",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/countries"
                  }
                }
              }
            }
          },
          "400": {
            "description": "bad input parameter"
          }
        }
    }
}