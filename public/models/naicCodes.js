exports.naicCodes = {
    "get": {
        "tags": [
          "lookups"
        ],
        "summary": "Fetch NAIC Codes",
        "description": "Get the list of NAIC codes",
        "operationId": "naicCodes",
        "responses": {
          "200": {
            "description": "List of NAIC codes",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/naicCodes"
                  }
                }
              }
            }
          },
          "400": {
            "description": "bad input parameter"
          }
        }
    }
}