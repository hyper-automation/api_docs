exports.productType = {
    "get": {
        "tags": [
          "lookups"
        ],
        "summary": "Fetch Product Type",
        "description": "Get the list of product type",
        "operationId": "productType",
        "responses": {
          "200": {
            "description": "List of product type",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/productType"
                  }
                }
              }
            }
          },
          "400": {
            "description": "bad input parameter"
          }
        }
    }
}