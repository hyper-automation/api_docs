exports.injuryCodes = {
    "get": {
        "tags": [
          "lookups"
        ],
        "summary": "Fetch Injury Codess",
        "description": "Get the list of injury codes",
        "operationId": "injuryCodes",
        "responses": {
          "200": {
            "description": "List of injury codes",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/injuryCodes"
                  }
                }
              }
            }
          },
          "400": {
            "description": "bad input parameter"
          }
        }
    }
}