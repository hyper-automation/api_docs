exports.maritalStatus = {
    "get": {
        "tags": [
          "lookups"
        ],
        "summary": "Fetch Marital Status List",
        "description": "Get the list of marital status",
        "operationId": "maritalStatus",
        "responses": {
          "200": {
            "description": "List of marital status",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/maritalStatus"
                  }
                }
              }
            }
          },
          "400": {
            "description": "bad input parameter"
          }
        }
    }
}