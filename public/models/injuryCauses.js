exports.injuryCauses = {
    "get": {
        "tags": [
          "lookups"
        ],
        "summary": "Fetch Injury Causes",
        "description": "Get the list of injury causes",
        "operationId": "injuryCauses",
        "responses": {
          "200": {
            "description": "List of injury causes",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/injuryCauses"
                  }
                }
              }
            }
          },
          "400": {
            "description": "bad input parameter"
          }
        }
    }
}