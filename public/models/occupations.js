exports.occupations = {
    "get": {
        "tags": [
          "lookups"
        ],
        "summary": "Fetch Occupation List",
        "description": "Get the list of avilable  occupations",
        "operationId": "occupations",
        "responses": {
          "200": {
            "description": "List of avilable  occupations",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/occupations"
                  }
                }
              }
            }
          },
          "400": {
            "description": "bad input parameter"
          }
        }
    }
}