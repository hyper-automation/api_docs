exports.claimStatus = {
    "get": {
        "tags": [
          "lookups"
        ],
        "summary": "Fetch Claim Status",
        "description": "Get the list of claim status",
        "operationId": "claimStatus",
        "responses": {
          "200": {
            "description": "List of claim status",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/claimStatus"
                  }
                }
              }
            }
          },
          "400": {
            "description": "bad input parameter"
          }
        }
    }
}