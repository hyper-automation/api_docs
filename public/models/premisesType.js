exports.premisesType = {
    "get": {
        "tags": [
          "lookups"
        ],
        "summary": "Fetch Premises Type",
        "description": "Get the list of premises type",
        "operationId": "premisesType",
        "responses": {
          "200": {
            "description": "List of premises type",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/premisesType"
                  }
                }
              }
            }
          },
          "400": {
            "description": "bad input parameter"
          }
        }
    }
}