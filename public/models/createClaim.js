
exports.createClaim = {
    "post": {
        "tags": [
          "Claims"
        ],
        "summary": "To create new claim",
        "description": "Create a new claim and return the claim Id.",
        "operationId": "createClaim",
        "requestBody": {
          "description": "Claim data",
          "content": {
            "application/json": {
              "schema": {
                  "oneOf":[
                    {"$ref": "#/components/schemas/GL_createClaim"},
                    {"$ref": "#/components/schemas/WC_createClaim"}
                  ]
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Your claim is created and cliam Id is 12344, save it for future reference."
          },
          "400": {
            "description": "invalid input, object invalid"
          }
        }
      }
}