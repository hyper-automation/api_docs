exports.wagesFrequency = {
    "get": {
        "tags": [
          "lookups"
        ],
        "summary": "Fetch Wages Frequency",
        "description": "Get the list of Wages Frequency",
        "operationId": "wagesFrequency",
        "responses": {
          "200": {
            "description": "List of Wages Frequency",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/wagesFrequency"
                  }
                }
              }
            }
          },
          "400": {
            "description": "bad input parameter"
          }
        }
    }
}