exports.bodyCodes = {
    "get": {
        "tags": [
          "lookups"
        ],
        "summary": "Fetch Body Codes",
        "description": "Get the list of body codes",
        "operationId": "bodyCodes",
        "responses": {
          "200": {
            "description": "List of body codes",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/bodyCodes"
                  }
                }
              }
            }
          },
          "400": {
            "description": "bad input parameter"
          }
        }
    }
}