exports.documentType = {
    "get": {
        "tags": [
          "lookups"
        ],
        "summary": "Fetch Document Types",
        "description": "Get the list of document type",
        "operationId": "documentType",
        "responses": {
          "200": {
            "description": "List of document type",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/documentType"
                  }
                }
              }
            }
          },
          "400": {
            "description": "bad input parameter"
          }
        }
    }
}