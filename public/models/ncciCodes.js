exports.ncciCodes = {
    "get": {
        "tags": [
          "lookups"
        ],
        "summary": "Fetch NCCI Codes",
        "description": "Get the list of NCCI codes",
        "operationId": "ncciCodes",
        "responses": {
          "200": {
            "description": "List of NCCI codes",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/ncciCodes"
                  }
                }
              }
            }
          },
          "400": {
            "description": "bad input parameter"
          }
        }
    }
}