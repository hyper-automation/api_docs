exports.states = {
    "get": {
        "tags": [
          "lookups"
        ],
        "summary": "Fetch States",
        "description": "Get the list of states",
        "operationId": "states",
        "parameters": [
            {
              "name": "country",
              "in": "query",
              "description": "Pass the countryID to get related states",
              "required": true,
              "style": "form",
              "explode": true,
              "schema": {
                "type": "string"
              }
            }
          ],
        "responses": {
          "200": {
            "description": "List of states",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/states"
                  }
                }
              }
            }
          },
          "400": {
            "description": "bad input parameter"
          }
        }
    }
}