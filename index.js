'use strict'


const express = require('express');
const app = express();
const config = require('config');
const swaggerUi = require('swagger-ui-express');
const swaggerOptions = require(`./swagger/${config.get("environment")}`);
const port = process.env.PORT || 3000;

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerOptions[config.get("environment")]));


app.listen(port, () => {
    console.log(`Server Started at ${new Date()}`)
    console.log(`Api doc will open at http://localhost:${port}/api-docs`);
});