claimDetails = {
    "claimID": "",
    "claimType": "lookupId",
    "policyNumber": "",
    "claimStatus": "lookupId",
    "claimSourceType": "lookupId",
    "claimCreatedBy": "",
    "claimCreatedOn": ""
}

documentDetails = {
    "documentID" : "",
    "claimID" : "reference",
    "documentTitle" : "",
    "documentURL" : "",
    "documentType" : "lookupId",
    "documentFormat" : ""
}

WC_occuranceDetails = {
    "occuranceID" : "",
    "claimID" : "refence",
    "address" : {
        "addressID":"",
        "addressName":"",
        "addressCity":"",
        "addressState":"lookupId",
        "addressCountry":"lookupId",
        "addressZip":""
    },
    "lastWorkDate" : "",
    "employerNotifiedDate" : "",
    "didOccuredOnEmpPremises" : "",
    "occurredDepartment" : "",
    "equipmentList" : "",
    "specificActivity" : "",
    "safetyEqupProvided" : "",
    "safetyEqupUsed" : "",
    "occurrenceDetails" : "",
    "workProcess" : "",
    "eventSequence" : "",
    "ocurrenceTime" : "",
}

GL_occuranceDetails = {
    "occuranceID": "",
    "claimID": "refence",
    "address": {
        "addressID":"",
        "addressName":"",
        "addressCity":"",
        "addressState":"lookupId",
        "addressCountry":"lookupId",
        "addressZip":""
    },
    "employerNotifiedDate": "",
    "occurredDepartment": "",
    "equipmentList": "",
    "specificActivity": "",
    "policeContacted": "",
    "reportNumber": "",
    "occurrenceDetails": "",
    "eventSequence": "",
    "ocurrenceTime": ""
}


injuryDetails = {
    "injuryID": "",
    "claimID": "refence",
    "injuryDate": "",
    "injuryCause": "lookupId",
    "injuryType": "lookupId",
    "bodyCode": "lookupId",
    "fatalStatus": "",
    "deathDate": "",
    "beginWork": "",
    "returnDate": "",
    "disabilityBeganDate": "",
}


treatmentDetails = {
    "treatmentID": "",
    "claimID": "refence",
    "physicianName": "",
    "address": {
        "addressID":"",
        "addressName":"",
        "addressCity":"",
        "addressState":"lookupId",
        "addressCountry":"lookupId",
        "addressZip":""
    },
    "hospName": "",
    "initialTreatment": "",
    "administratorNotifiedDate": "",
}

GL_liabilityDetails = {
    "liabilityID":"",
    "claimID": "refence",
    "premisesInsured": "",
    "ownerName": "",
    "address": {
        "addressID":"",
        "addressName":"",
        "addressCity":"",
        "addressState":"lookupId",
        "addressCountry":"lookupId",
        "addressZip":""
    },
    "productInsured": "",
    "whereBeSeen": "",
    "typeOfPremises": "",
    "phone": "",
    "email": "",
    "vendor": {
        "manufacturerID": "",
        "manufacturerName": "",
        "address": {
            "addressID":"",
            "addressName":"",
            "addressCity":"",
            "addressState":"lookupId",
            "addressCountry":"lookupId",
            "addressZip":""
        },
        "productType": "lookupId",
        "premisesType": "lookupId",
        "phone": "",
        "email": ""
    },
    "manufactuter": {
        "vendorID": "",
        "vendorName": "",
        "address": {
            "addressID":"",
            "addressName":"",
            "addressCity":"",
            "addressState":"lookupId",
            "addressCountry":"lookupId",
            "addressZip":""
        },
        "productType": "lookupId",
        "premisesType": "lookupId",
        "phone": "",
        "email": ""
    }
}


GL_injuredDetails = {
    "injuredID": "",
    "claimID": "refence",
    "firstName": "",
    "lastName": "",
    "dob": "",
    "age": "",
    "sex": "",
    "maritalstatus": "lookupId",
    "phone": "",
    "email": "",
    "address": {
        "addressID":"",
        "addressName":"",
        "addressCity":"",
        "addressState":"lookupId",
        "addressCountry":"lookupId",
        "addressZip":""
    }
}

propertyDetails = {
    "propertyID": "",
    "claimID": "refence",
    "ownerName": "",
    "address": {
        "addressID":"",
        "addressName":"",
        "addressCity":"",
        "addressState":"lookupId",
        "addressCountry":"lookupId",
        "addressZip":""
    },
    "phone": "",
    "email": "",
    "describeProperty": "",
    "estimateAmount": "",
    "propertyLocation": "",
}

WC_claimantDetails = {
    "claimantID":"",
    "ClaimID":"refence",
    "firstName":"",
    "lastName":"",
    "dob":"",
    "age":"",
    "sex":"",
    "maritalstatus":"lookupId",
    "occupation":"lookupId",
    "employementStatus":"",
    "feinNumber":"",
    "primaryPhone":"",
    "secondaryPhone":"",
    "primaryEmail":"",
    "secondaryEmail":"",
    "numOfDependents":"",
    "employeeCode":"",
    "SSN_Number":"",
    "dateHired":"",
    "stateHired":"",
    "ncciCodeID":"lookupId",
    "wageRate":"",
    "wageFrequencyID":"lookupId",
    "avgWeeklyWages":"",
    "noOfDaysPerWeek":"",
    "fullPayDayOfInjury":"",
    "salaryStatus":"",
    "whereTaken":"",
}


GL_claimantDetails	= {
    "claimantID": "",
    "ClaimID": "refence",
    "firstName": "",
    "lastName": "",
    "dob": "",
    "age": "",
    "sex": "",
    "maritalstatus": "lookupId",
    "occupation": "lookupId",
    "employementStatus": "",
    "feinNumber": "",
    "primaryPhone": "",
    "secondaryPhone": "",
    "primaryEmail": "",
    "secondaryEmail": "",
    "numOfDependents": "",
    "whereTaken": ""
}


witnessDetails = {
    "witnessID": "",
    "claimID": "refence",
    "witnessName": "",
    "address": {
        "addressID":"",
        "addressName":"",
        "addressCity":"",
        "addressState":"lookupId",
        "addressCountry":"lookupId",
        "addressZip":""
    },
    "phone": "",
    "email": ""
}
