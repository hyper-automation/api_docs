

//createClaim
    //Post
    
    requestBody = {
        "claimDetails" : {
            "claimType": "lookupId",
            "claimStatus": "lookupId",
            "claimSourceType": "lookupId"
        },
        "documentDetails":[
            {
                "documentTitle" : "",
                "documentURL" : "response of uploaded api",
                "documentType" : "lookupId",
                "documentFormat" : ""
            }
        ],
        "WC_occuranceDetails": {
            "address" : {
                "addressName":"",
                "addressCity":"",
                "addressState":"lookupId",
                "addressCountry":"lookupId",
                "addressZip":""
            },
            "lastWorkDate" : "",
            "employerNotifiedDate" : "",
            "didOccuredOnEmpPremises" : "",
            "occurredDepartment" : "",
            "equipmentList" : "",
            "specificActivity" : "",
            "safetyEqupProvided" : "",
            "safetyEqupUsed" : "",
            "occurrenceDetails" : "",
            "workProcess" : "",
            "eventSequence" : "",
            "ocurrenceTime" : "",
        },
        "injuryDetails": {
            "injuryDate": "",
            "injuryCause": "lookupId",
            "injuryType": "lookupId",
            "bodyCode": "lookupId",
            "fatalStatus": "",
            "deathDate": "",
            "beginWork": "",
            "returnDate": "",
            "disabilityBeganDate": "",
        },
        "treatmentDetails": {
            "physicianName": "",
            "address": {
                "addressName":"",
                "addressCity":"",
                "addressState":"lookupId",
                "addressCountry":"lookupId",
                "addressZip":""
            },
            "hospName": "",
            "initialTreatment": "",
            "administratorNotifiedDate": "",
        },
        "propertyDetails": {
            "ownerName": "",
            "address": {
                "addressName":"",
                "addressCity":"",
                "addressState":"lookupId",
                "addressCountry":"lookupId",
                "addressZip":""
            },
            "phone": "",
            "email": "",
            "describeProperty": "",
            "estimateAmount": "",
            "propertyLocation": "",
        },
        "WC_claimantDetails": {
            "firstName":"",
            "lastName":"",
            "dob":"",
            "age":"",
            "sex":"",
            "maritalstatus":"lookupId",
            "occupation":"lookupId",
            "employementStatus":"",
            "feinNumber":"",
            "primaryPhone":"",
            "secondaryPhone":"",
            "primaryEmail":"",
            "secondaryEmail":"",
            "numOfDependents":"",
            "employeeCode":"",
            "SSN_Number":"",
            "dateHired":"",
            "stateHired":"",
            "ncciCodeID":"lookupId",
            "wageRate":"",
            "wageFrequencyID":"lookupId",
            "avgWeeklyWages":"",
            "noOfDaysPerWeek":"",
            "fullPayDayOfInjury":"",
            "salaryStatus":"",
            "whereTaken":"",
        },
        "witnessDetails": [{
            "witnessName": "",
            "address": {
                "addressName":"",
                "addressCity":"",
                "addressState":"lookupId",
                "addressCountry":"lookupId",
                "addressZip":""
            },
            "phone": "",
            "email": ""
        }]

    }





//updateClaim
    //Put
     /* In case of update only changed value will be sent in request body */
    
    requestBody = {
        "claimDetails" : {
            "claimID": "",
        },
        "documentDetails":[
            {
                "documentID" : "",
                "documentTitle" : "",
                "documentURL" : "response of uploaded api",
                "documentType" : "lookupId",
                "documentFormat" : ""
            }
        ],
        "WC_occuranceDetails": {
            "occuranceID" : "",
            "address" : {
                "addressID":"",
                "addressName":"",
                "addressCity":"",
                "addressState":"lookupId",
                "addressCountry":"lookupId",
                "addressZip":""
            },
            "lastWorkDate" : "",
            "employerNotifiedDate" : "",
            "didOccuredOnEmpPremises" : "",
            "occurredDepartment" : "",
            "equipmentList" : "",
            "specificActivity" : "",
            "safetyEqupProvided" : "",
            "safetyEqupUsed" : "",
            "occurrenceDetails" : "",
            "workProcess" : "",
            "eventSequence" : "",
            "ocurrenceTime" : "",
        },
        "injuryDetails": {
            "injuryID": "",
            "injuryDate": "",
            "injuryCause": "lookupId",
            "injuryType": "lookupId",
            "bodyCode": "lookupId",
            "fatalStatus": "",
            "deathDate": "",
            "beginWork": "",
            "returnDate": "",
            "disabilityBeganDate": "",
        },
        "treatmentDetails": {
            "treatmentID": "",
            "physicianName": "",
            "address": {
                "addressID":"",
                "addressName":"",
                "addressCity":"",
                "addressState":"lookupId",
                "addressCountry":"lookupId",
                "addressZip":""
            },
            "hospName": "",
            "initialTreatment": "",
            "administratorNotifiedDate": "",
        },
        "propertyDetails": {
            "liabilityID":"",
            "ownerName": "",
            "address": {
                "addressID":"",
                "addressName":"",
                "addressCity":"",
                "addressState":"lookupId",
                "addressCountry":"lookupId",
                "addressZip":""
            },
            "phone": "",
            "email": "",
            "describeProperty": "",
            "estimateAmount": "",
            "propertyLocation": "",
        },
        "WC_claimantDetails": {
            "claimantID":"",
            "firstName":"",
            "lastName":"",
            "dob":"",
            "age":"",
            "sex":"",
            "maritalstatus":"lookupId",
            "occupation":"lookupId",
            "employementStatus":"",
            "feinNumber":"",
            "primaryPhone":"",
            "secondaryPhone":"",
            "primaryEmail":"",
            "secondaryEmail":"",
            "numOfDependents":"",
            "employeeCode":"",
            "SSN_Number":"",
            "dateHired":"",
            "stateHired":"",
            "ncciCodeID":"lookupId",
            "wageRate":"",
            "wageFrequencyID":"lookupId",
            "avgWeeklyWages":"",
            "noOfDaysPerWeek":"",
            "fullPayDayOfInjury":"",
            "salaryStatus":"",
            "whereTaken":"",
        },
        "witnessDetails": [{
            "witnessID": "",
            "witnessName": "",
            "address": {
                "addressID":"",
                "addressName":"",
                "addressCity":"",
                "addressState":"lookupId",
                "addressCountry":"lookupId",
                "addressZip":""
            },
            "phone": "",
            "email": ""
        }]

    }