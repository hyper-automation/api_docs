
//claimType
    //get
    response = [
        {
            "claimTypeID":"",
            "claimType":""
        }
    ]

//claimStatus
    //get
    response = [
        {
            "statusID":"",
            "status":""
        }
    ]

//sourceType
    //get
    response = [
        {
            "sourceTypeID":"",
            "sourceType":""
        }
    ]


//documentType
    //get
    response = [
        {
            "documentTypeID":"",
            "description":""
        }
    ]



//maritalStatus
    //get
    response = [
        {
            "maritalStatusID":"",
            "maritalStatus":""
        }
    ]


//occupationDetails
    //get
    response = [
        {
            "occupationID":"",
            "occupationDescription":""
        }
    ]


//wagesFrequency
    //get
    response = [
        {
            "wageFrequencyID":"",
            "wageDescription":""
        }
    ]


//injuryCodes
    //get
    response = [
        {
            "injuryCodeID":"",
            "injuryCode":""
        }
    ]



//naicCodes
    //get
    response = [
        {
            "naicCodeID":"",
            "naicCode":""
        }
    ]


//ncciCodes
    //get
    response = [
        {
            "ncciCodeID":"",
            "ncciCode":""
        }
    ]


//injuryCauses
    //get
    response = [
        {
            "injuryCauseID":"",
            "injuryCauseName":""
        }
    ]


//productType
    //get
    response = [
        {
            "productTypeID":"",
            "productType":""
        }
    ]


//premisesType
    //get
    response = [
        {
            "premisesTypeID":"",
            "premisesType":""
        }
    ]


//countries
    //get
    response = [
        {
            "countryID":"",
            "countryName":""
        }
    ]

//states?country={countryID}
    //get
    response = [
        {
            "stateID":"",
            "stateName":""
        }
    ]