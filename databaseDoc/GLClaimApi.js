

//createClaim
    //Post
    
    requestBody = {
        "claimDetails" : {
            "claimType": "lookupId",
            "claimStatus": "lookupId",
            "claimSourceType": "lookupId"
        },
        "documentDetails":[
            {
                "documentTitle" : "",
                "documentURL" : "response of uploaded api",
                "documentType" : "lookupId",
                "documentFormat" : ""
            }
        ],
        "GL_occuranceDetails": {
            "address": {
                "addressName":"",
                "addressCity":"",
                "addressState":"lookupId",
                "addressCountry":"lookupId",
                "addressZip":""
            },
            "employerNotifiedDate": "",
            "occurredDepartment": "",
            "equipmentList": "",
            "specificActivity": "",
            "policeContacted": "",
            "reportNumber": "",
            "occurrenceDetails": "",
            "eventSequence": "",
            "ocurrenceTime": ""
        },
        "injuryDetails": {
            "injuryDate": "",
            "injuryCause": "lookupId",
            "injuryType": "lookupId",
            "bodyCode": "lookupId",
            "fatalStatus": "",
            "deathDate": "",
            "beginWork": "",
            "returnDate": "",
            "disabilityBeganDate": "",
        },
        "treatmentDetails": {
            "physicianName": "",
            "address": {
                "addressName":"",
                "addressCity":"",
                "addressState":"lookupId",
                "addressCountry":"lookupId",
                "addressZip":""
            },
            "hospName": "",
            "initialTreatment": "",
            "administratorNotifiedDate": "",
        },
        "GL_liabilityDetails": {
            "premisesInsured": "",
            "ownerName": "",
            "address": {
                "addressName":"",
                "addressCity":"",
                "addressState":"lookupId",
                "addressCountry":"lookupId",
                "addressZip":""
            },
            "productInsured": "",
            "whereBeSeen": "",
            "typeOfPremises": "",
            "phone": "",
            "email": "",
            "vendor": {
                "manufacturerName": "",
                "address": {
                    "addressName":"",
                    "addressCity":"",
                    "addressState":"lookupId",
                    "addressCountry":"lookupId",
                    "addressZip":""
                },
                "productType": "lookupId",
                "premisesType": "lookupId",
                "phone": "",
                "email": ""
            },
            "manufactuter": {
                "vendorName": "",
                "address": {
                    "addressName":"",
                    "addressCity":"",
                    "addressState":"lookupId",
                    "addressCountry":"lookupId",
                    "addressZip":""
                },
                "productType": "lookupId",
                "premisesType": "lookupId",
                "phone": "",
                "email": ""
            }
        },
        "GL_injuredDetails": {
            "firstName": "",
            "lastName": "",
            "dob": "",
            "age": "",
            "sex": "",
            "maritalstatus": "lookupId",
            "phone": "",
            "email": "",
            "address": {
                "addressName":"",
                "addressCity":"",
                "addressState":"lookupId",
                "addressCountry":"lookupId",
                "addressZip":""
            }
        },
        "propertyDetails": {
            "ownerName": "",
            "address": {
                "addressName":"",
                "addressCity":"",
                "addressState":"lookupId",
                "addressCountry":"lookupId",
                "addressZip":""
            },
            "phone": "",
            "email": "",
            "describeProperty": "",
            "estimateAmount": "",
            "propertyLocation": "",
        },
        "witnessDetails": [{
            "witnessName": "",
            "address": {
                "addressName":"",
                "addressCity":"",
                "addressState":"lookupId",
                "addressCountry":"lookupId",
                "addressZip":""
            },
            "phone": "",
            "email": ""
        }]

    }