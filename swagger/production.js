const config = require('config');
const swaggerConst = config.get('swagger');
const { path }  = require('../public/apis');
const { schema }  = require('../schemas/index');


exports.production =  {
    "openapi": "3.0.0",
    "info": swaggerConst["info"],
    "servers": swaggerConst["servers"],
    "tags": [
      {
        "name": "Claims",
        "description": "Claims API's"
      },
      {
        "name": "lookups",
        "description": "Lookup table data for form"
      }
    ],
    "paths": path,
    "components": {
      "schemas": schema,
      "securitySchemes": {
        "basicAuth": {
          "type":   'http',
          "scheme": 'basic'
        }
    }
  }
}

